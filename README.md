### SETUPS
1. Java JDK: https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
2. Allure: https://github.com/allure-framework/allure2/releases

### SETTINGS
1. JAVA_HOME: /Java/jdkXXX
2. Allure >> /Allure/bin

#### Test result of this sample project:
https://bitbucket.org/ngoanh9n/apply-neutrino-java-sample/src/master/images/test-report.png

### PROJECT STRUCTURE
```
libs/
  |--neutrino-java.jar
src/
  |--test/
    |--java/
      |--pc.sample/
        |--actions/
          |--SignIn.java
          |--VerifyButton.java
          |--VerifyLinkButton.java
          |--VerifyTextBox.java
        |--assets/
          |--Attribute.java
          |--Color.java
          |--Commons.java
          |--CSS.java
          |--Cursor.java
          |--FontSize.java
        |--repositories/
          |--components/
            |--Header.java
          |--pages/
            |--ProfilePage.java
            |--SignInPage.java
        |--scripts/
          |--ProfilePageScript.java
          |--SignInPageScript.java
    |--resources/
      |--data/
      　　|--Credentials.yml
      |--neutrino/
        |--configuration.yml
        |--log4j.properties
      |--neutrino.xml
```

### EXECUTIONS:
#### Run tests:
```java
<gradlew chrome>
<gradlew firefox>
```
#### See reports:
```java
<allure serve build/allure-results>
```

### CONFIGURATIONS:
```yaml
timeout:
  min: "3"
  max: "30"
web-driver:
  chrome: "2.37"
  gecko: "0.22.0"
web:
  headless: "false"
  url: "https://gitlab.com"
```

### ACTIONS (For Desktop Web application):
```java
 * Attack
 * Check
 * Clear
 * Click
 * Hover
 * Input
 * Press
 * Move
 * Select
 * Value
 * Verify
 * Wait
 * DoubleClick
 * RightClick
```

### TEST FRAMEWORKS USED:
```java
 * Test NG: 6.14.2
 * Selenium Java: 3.12.0
 * Appium Java Client: 6.1.0
 * Allure Test NG: 2.6.0
```

### LINKS:
1. RGB Calculator
https://www.w3schools.com/colors/colors_rgb.asp
2. Name of Color by code
http://chir.ag/projects/name-that-color/