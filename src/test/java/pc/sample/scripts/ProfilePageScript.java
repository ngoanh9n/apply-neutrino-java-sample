package pc.sample.scripts;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import neutrino.factory.builders.CaptureBuilder;
import neutrino.factory.builders.ChildBuilder;
import neutrino.factory.builders.ScenarioGifBuilder;
import neutrino.factory.generals.BaseTest;
import neutrino.files.FileReader;
import org.openqa.selenium.support.How;
import org.testng.annotations.Test;
import pc.sample.actions.SignIn;
import pc.sample.assets.Commons;
import pc.sample.repositories.pages.ProfilePage;
import pc.sample.repositories.pages.SignInPage;

import java.util.HashMap;

import static neutrino.Application.*;

@Epic(Commons.Titles.Epic)
@Feature(Commons.Titles.Feature)
public class ProfilePageScript extends BaseTest {

    private final String category = ProfilePage.class.getSimpleName();

    @Story(Commons.Titles.Story)
    @Description("プロフィルページテスト")
    @Test(description = "プロフィルページのデータbindingを確認します。")
    public void checkUserInfo() {

        // Setup info for scenario gif building
        // isEnabled(true)　→　スクリプトを実行してから、シナリオGifが作成される。
        ScenarioGifBuilder gif = this.scenarioGifBuilder.setCategory(category).setDelay(600).isLoop(true).isEnabled(false).build();

        // Setup info for step image capture
        // isAlways(true)　→　それぞれステップが終わった時、スクリーンショットをする。
        CaptureBuilder capture = this.captureBuilder.setCategory(category).isAlways(false).setScenarioGifAtt(gif).build();

        // Open login page with url included
        page(SignInPage.class).open();

        // Load test data
        HashMap credentials = FileReader.yml(Commons.Paths.CredentialsPath);

        // Sign in as valid
        String username = (String) credentials.get(Commons.Keys.Username);
        String password = (String) credentials.get(Commons.Keys.Password);
        action(SignIn.class).setParams(username, password).perform();

        // Go to Profile Page
        click.onLocator("btnUserToggle").attach(capture).perform();
        click.onLocator("itemProfile").attach(capture).perform();

        // Profile Page is navigated
        page(ProfilePage.class).isNavigated();

        // Check user info
        verify.onLocator("lblCoverTitle").visibleText().equalTo("Ho Huu Ngoan");
        verify.onLocator("lblCoverMember")
                .where(new ChildBuilder()
                        .bySelector(How.TAG_NAME, "span")
                        .byIndex(1)
                        .setAlias("Username Label"))
                .visibleText().equalTo("@ngoanh9n");
        verify.onLocator("lblCoverMember")
                .where(new ChildBuilder()
                        .bySelector(How.TAG_NAME, "span")
                        .byIndex(2)
                        .setAlias("Registered Date Label"))
                .attach(capture)
                .visibleText().equalTo("Member since November 24, 2018");
    }
}
