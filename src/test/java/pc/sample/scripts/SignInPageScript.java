package pc.sample.scripts;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import neutrino.factory.builders.CaptureBuilder;
import neutrino.factory.builders.ScenarioGifBuilder;
import neutrino.factory.generals.BaseTest;
import neutrino.files.FileReader;
import org.testng.annotations.Test;
import pc.sample.actions.VerifyButton;
import pc.sample.actions.VerifyLinkText;
import pc.sample.actions.VerifyTextBox;
import pc.sample.assets.Commons;
import pc.sample.repositories.pages.SignInPage;

import java.util.HashMap;

import static neutrino.Application.*;

@Epic(Commons.Titles.Epic)
@Feature(Commons.Titles.Feature)
public class SignInPageScript extends BaseTest {

    private final String category = SignInPage.class.getSimpleName();

    @Story(Commons.Titles.Story)
    @Description("ログインページテスト")
    @Test(description = "初期の状態を確認し、ログインします。")
    public void verifyAndSingIn() {

        // Setup info for scenario gif building
        // isEnabled(true)　→　スクリプトを実行してから、シナリオGifが作成される。
        ScenarioGifBuilder gif = this.scenarioGifBuilder.setCategory(category).setDelay(600).isLoop(true).isEnabled(false).build();

        // Setup info for step image capture
        // isAlways(true)　→　それぞれステップが終わった時、スクリーンショットをする。
        CaptureBuilder capture = this.captureBuilder.setCategory(category).isAlways(false).setScenarioGifAtt(gif).build();

        // Open login page with url included
        page(SignInPage.class).open();

        // Verify Username TextBox with type: text
        action(VerifyTextBox.class).setParams("txtUsername", "text").perform();
        // Verify Password TextBox with type: password
        action(VerifyTextBox.class).setParams("txtPassword", "password").perform();
        // Verify Forgot Your Password has visible text: Forgot your password?
        action(VerifyLinkText.class).setParams("lnkForgot", "Forgot your password?").perform();
        // Verify Login Button has visible text: Login
        action(VerifyButton.class).setParams("btnLogin", "Sign in").perform();

        // Load test data
        HashMap credentials = FileReader.yml(Commons.Paths.CredentialsPath);

        // Login with credentials
        input.onLocator("txtUsername")
                .attach("これは、ユーザー名をインプットのステップ")
                .attach(capture)
                .with((String) credentials.get(Commons.Keys.Username))
                .perform();
        input.onLocator("txtPassword")
                .attach("これは、パスワードをインプットのステップ")
                .attach(capture)
                .with((String) credentials.get(Commons.Keys.Password))
                .perform();
        click.onLocator("btnLogin").attach(capture).perform();
    }
}
