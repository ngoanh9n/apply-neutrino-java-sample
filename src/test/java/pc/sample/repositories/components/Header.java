package pc.sample.repositories.components;

/*
 * @author: Ho Huu Ngoan
 * */

import neutrino.annotations.Component;
import neutrino.annotations.Locator;
import org.openqa.selenium.support.How;

@Component
public class Header {

    @Locator(how = How.CSS, using = "a[class='header-user-dropdown-toggle']", alias = "ユーザートグル")
    private String btnUserToggle;

    @Locator(how = How.CSS, using = "a[class='profile-link']", alias = "プロフィールアイテム")
    private String itemProfile;
}
