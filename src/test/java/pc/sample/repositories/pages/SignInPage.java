package pc.sample.repositories.pages;

/*
* @author: Ho Huu Ngoan
* */

import neutrino.annotations.Locator;
import neutrino.annotations.Page;
import neutrino.annotations.Using;
import neutrino.factory.generals.BasePage;
import org.openqa.selenium.support.How;
import pc.sample.repositories.components.Header;

@Using(components = {Header.class})
@Page(url = "/users/sign_in", alias = "ログインページ")
public class SignInPage extends BasePage {

    @Locator(how = How.ID, using = "user_login", alias = "ユーザー名")
    private String txtUsername;

    @Locator(how = How.ID, using = "user_password", alias = "パスワード")
    private String txtPassword;

    @Locator(how = How.CSS, using = "a[href='/users/password/new']", alias = "忘れたパスワードリンク")
    private String lnkForgot;

    @Locator(how = How.CSS, using = "input[class='btn btn-success qa-sign-in-button']", alias = "ログインボタン")
    private String btnLogin;
}
