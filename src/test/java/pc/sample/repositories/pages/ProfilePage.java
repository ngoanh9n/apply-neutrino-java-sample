package pc.sample.repositories.pages;

/*
 * @author: Ho Huu Ngoan
 * */

import neutrino.annotations.Locator;
import neutrino.annotations.Page;
import neutrino.annotations.Using;
import neutrino.factory.generals.BasePage;
import org.openqa.selenium.support.How;
import pc.sample.repositories.components.Header;

@Using(components = {Header.class})
@Page(alias = "プロフィルページ")
public class ProfilePage extends BasePage {

    @Locator(how = How.CSS, using = "div[class='cover-title']", alias = "表題タイトルラベル")
    private String lblCoverTitle;

    @Locator(how = How.CSS, using = "div[class='cover-desc member-date']", alias = "カバーメンバーラベル")
    private String lblCoverMember;
}
