package pc.sample.assets;

/*
 * @author: Ho Huu Ngoan
 * */

public final class CSS {

    public final static String Cursor = "cursor";
    public final static String FontSize = "font-size";
    public final static String BackgroundColor = "background-color";
    public final static String BorderButtonColor = "border-bottom-color";
    public final static String TextDecorationColor = "text-decoration-color";
}
