package pc.sample.assets;

/*
 * @author: Ho Huu Ngoan
 * */

public final class Cursor {

    public final static String Text = "text";
    public final static String Pointer = "pointer";
    public final static String Default = "default";
}
