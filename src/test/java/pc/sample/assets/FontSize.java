package pc.sample.assets;

/*
 * @author: Ho Huu Ngoan
 * */

public final class FontSize {

    public final static String _12 = "12px";
    public final static String _13 = "13px";
    public final static String _14 = "14px";
}
