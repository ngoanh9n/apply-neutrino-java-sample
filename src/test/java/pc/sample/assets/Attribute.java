package pc.sample.assets;

/*
 * @author: Ho Huu Ngoan
 * */

public final class Attribute {

    public final static String Type = "type";
    public final static String Clazz = "class";
    public final static String PlaceHolder = "placeholder";
}
