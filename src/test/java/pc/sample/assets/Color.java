package pc.sample.assets;

/*
 * @author: Ho Huu Ngoan
 * */

public final class Color {

    public final static String White = "255, 255, 255";
    public final static String MountainMeadow = "26, 170, 85";
    public final static String Jewel = "22, 143, 72";
    public final static String Denim = "27, 105, 182";

//    public final static String White = "rgba(255, 255, 255, 1)";
//    public final static String MountainMeadow = "rgba(26, 170, 85, 1)";
//    public final static String Jewel = "rgba(22, 143, 72, 1)";
//    public final static String Denim = "rgba(27, 105, 182, 1)";
}
