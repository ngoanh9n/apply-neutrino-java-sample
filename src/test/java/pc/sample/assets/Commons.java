package pc.sample.assets;

import neutrino.data.Container;

/*
 * @author: Ho Huu Ngoan
 * */

public final class Commons {

    public final class Titles {

        public final static String Epic = "Epic: GitLabデモ";
        public final static String Feature = "Feature: GitLabデモ";
        public final static String Story = "Story: GitLabデモ";
    }

    public final class Keys {

        public final static String Username = "username";
        public final static String Password = "password";
    }

    public static class Paths {

        public final static String CredentialsPath = Container.FolderPaths.RESOURCES + "/data/Credentials.yml";
    }
}
