package pc.sample.actions;

import neutrino.factory.generals.Action;
import neutrino.interfaces.perform.IPerform;

import static neutrino.Application.click;
import static neutrino.Application.input;

/*
 * @author: Ho Huu Ngoan
 * */

public class SignIn extends Action<IPerform<SignIn>> {

    @Override
    public IPerform<SignIn> setParams(Object... params) {
        super.setParams(params);

        return () -> {
            Object[] data = SignIn.super.getParams();
            String username = (String) data[0];
            String password = (String) data[1];

            input.onLocator("txtUsername").with(username).perform();
            input.onLocator("txtPassword").with(password).perform();
            click.onLocator("btnLogin").perform();

            return this;
        };
    }
}
