package pc.sample.actions;

import neutrino.factory.generals.Action;
import neutrino.interfaces.perform.IPerform;
import pc.sample.assets.*;

import static neutrino.Application.verify;

/*
 * @author: Ho Huu Ngoan
 * */

public class VerifyTextBox extends Action<IPerform<VerifyTextBox>> {

    @Override
    public IPerform<VerifyTextBox> setParams(Object... params) {
        super.setParams(params);

        return () -> {
            Object[] data = VerifyTextBox.super.getParams();
            String textBox = (String) data[0];
            String textBoxType = (String) data[1];

            verify.onLocator(textBox).status().isEnabled();
            verify.onLocator(textBox).onCss(CSS.Cursor).equalTo(Cursor.Text);
            verify.onLocator(textBox).onCss(CSS.FontSize).equalTo(FontSize._14);
            verify.onLocator(textBox).onCss(CSS.BackgroundColor).contains(Color.White);
            verify.onLocator(textBox).onAttribute(Attribute.Type).equalTo(textBoxType);

            return this;
        };
    }
}
