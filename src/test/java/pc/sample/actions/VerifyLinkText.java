package pc.sample.actions;

import neutrino.factory.generals.Action;
import neutrino.interfaces.perform.IPerform;
import pc.sample.assets.CSS;
import pc.sample.assets.Color;
import pc.sample.assets.Cursor;
import pc.sample.assets.FontSize;

import static neutrino.Application.verify;

/*
 * @author: Ho Huu Ngoan
 * */

public class VerifyLinkText extends Action<IPerform<VerifyLinkText>> {

    @Override
    public IPerform<VerifyLinkText> setParams(Object... params) {
        super.setParams(params);

        return () -> {
            Object[] data = VerifyLinkText.super.getParams();
            String linkText = (String) data[0];
            String linkVisibleText = (String) data[1];

            verify.onLocator(linkText).status().isEnabled();
            verify.onLocator(linkText).visibleText().equalTo(linkVisibleText);
            verify.onLocator(linkText).onCss(CSS.FontSize).equalTo(FontSize._13);
            verify.onLocator(linkText).onCss(CSS.TextDecorationColor).contains(Color.Denim);
            verify.onLocator(linkText).onCss(CSS.Cursor).equalTo(Cursor.Pointer);

            return this;
        };
    }
}
