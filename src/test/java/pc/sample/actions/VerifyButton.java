package pc.sample.actions;

import neutrino.factory.generals.Action;
import neutrino.interfaces.perform.IPerform;
import pc.sample.assets.CSS;
import pc.sample.assets.Color;
import pc.sample.assets.Cursor;
import pc.sample.assets.FontSize;

import static neutrino.Application.verify;

/*
 * @author: Ho Huu Ngoan
 * */

public class VerifyButton extends Action<IPerform<VerifyButton>> {

    @Override
    public IPerform<VerifyButton> setParams(Object... params) {
        super.setParams(params);

        return () -> {
            Object[] data = VerifyButton.super.getParams();
            String button = (String) data[0];
            String buttonVisibleText = (String) data[1];

            verify.onLocator(button).status().isEnabled();
            verify.onLocator(button).onAttribute("value").equalTo(buttonVisibleText);
            verify.onLocator(button).onCss(CSS.Cursor).equalTo(Cursor.Pointer);
            verify.onLocator(button).onCss(CSS.FontSize).equalTo(FontSize._14);
            verify.onLocator(button).onCss(CSS.BackgroundColor).contains(Color.MountainMeadow);
            verify.onLocator(button).onCss(CSS.BorderButtonColor).contains(Color.Jewel);

            return this;
        };
    }
}
